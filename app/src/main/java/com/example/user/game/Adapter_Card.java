package com.example.user.game;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

import java.util.List;

/**
 * Created by user on 2016/8/3.
 */
public class Adapter_Card extends RecyclerView.Adapter<Adapter_Card.ViewHolder> {
    List list;
    Context context;
    public Adapter_Card(NewGame newGame, List img_url) {
        list =img_url;
        context=newGame;
    }

    @Override
    public Adapter_Card.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_adapter, parent, false);
        return new ViewHolder(v);
    }
    public class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView imageView;
        TextView textView;
        public ViewHolder(View v) {
            super(v);
            imageView = (ImageView) v.findViewById(R.id.image_card);
//            textView=(TextView) v.findViewById(R.id.img_res);
        }
    }
    @Override
    public void onBindViewHolder(ViewHolder holder,int position) {
        String url="http://kevin84322.raguhn.tw/webgame/images/poker_cards/"
                +list.get(position)+".png";
        Picasso.with(context)
                .load(url)
                .transform(new ImgSize())
                .into(holder.imageView);
    }
    public class ImgSize implements Transformation {
        @Override
        public Bitmap transform(Bitmap source) {
            int x = source.getWidth()/ 2;
            int y = source.getHeight()/ 2;
            Bitmap result = Bitmap.createScaledBitmap(source,x, y,false);
            if (result != source) {
                source.recycle();
            }
            return result;
        }

        @Override public String key() { return "square()"; }
    }
    @Override
    public int getItemCount() {
        return list.size();
    }
}
