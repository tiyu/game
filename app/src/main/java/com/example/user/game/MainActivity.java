package com.example.user.game;

import android.content.Intent;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpCookie;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.MessageDigest;
import java.util.List;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;


public class MainActivity extends AppCompatActivity {

    Button button;
    EditText acc;
    EditText pwd;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setTitle("聽說這是21點...");
        button=(Button)findViewById(R.id.enter);
        acc=(EditText) findViewById(R.id.text_user);
        pwd=(EditText) findViewById(R.id.text_pwd);

        //Thread錯誤糾正方式
        StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
                .detectDiskReads()
                .detectDiskWrites()
                .detectNetwork()   // or .detectAll() for all detectable problems
                .penaltyLog()
                .build());
        StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder()
                .detectLeakedSqlLiteObjects()
                .detectLeakedClosableObjects()
                .penaltyLog()
                .penaltyDeath()
                .build());
    }

    @Override
    protected void onResume() {
        super.onResume();
        acc.setText("");
        pwd.setText("");
        button.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View view) {
                String user=acc.getText().toString();
                final String pass=MD5(pwd.getText().toString());
                try {
                    PHPSessId(user, pass);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void PHPSessId(final String user, String pass) throws IOException {
        final String COOKIES_HEADER = "Set-Cookie";

        String urlString = "http://kevin84322.raguhn.tw/webgame/api/login.php";
        HttpURLConnection connection = null;

        try {
            // 初始化 URL
            URL url = new URL(urlString);
            // 取得連線物件
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");

            String data = "account=" + user + "&password=" + pass;
            OutputStream out = connection.getOutputStream();// 获得一个输出流,向服务器写数据
            out.write(data.getBytes());
            out.flush();
            out.close();

            // 若要求回傳 200 OK 表示成功取得網頁內容
            if(connection.getResponseCode() == HttpsURLConnection.HTTP_OK ){
                java.net.CookieManager msCookieManager = new java.net.CookieManager();

                Map<String, List<String>> headerFields = connection.getHeaderFields();
                List<String> cookiesHeader = headerFields.get(COOKIES_HEADER);

                if(cookiesHeader != null){
                    for (String cookie : cookiesHeader){
                        msCookieManager.getCookieStore().add(null, HttpCookie.parse(cookie).get(0));
                    }
                }
                String[] Sid=cookiesHeader.toString().split("PHPSESSID=|; path=/");
                String PHPid=Sid[1];

                // 讀取網頁內容
                InputStream     inputStream     = connection.getInputStream();
                BufferedReader  bufferedReader  = new BufferedReader( new InputStreamReader(inputStream) );

                String tempStr;
                StringBuffer stringBuffer = new StringBuffer();

                while( ( tempStr = bufferedReader.readLine() ) != null ) {
                    stringBuffer.append( tempStr );
                }
                bufferedReader.close();
                inputStream.close();

                // 網頁內容字串
                String responseString = stringBuffer.toString();
                login ans = new Gson().fromJson(responseString, login.class);
                switch (ans.check){
                    case -33:
                        Toast.makeText(MainActivity.this, "帳號密碼空白", Toast.LENGTH_SHORT).show();
                        onResume();
                    case -87:
                        Toast.makeText(MainActivity.this, "帳號密碼錯誤", Toast.LENGTH_SHORT).show();
                        onResume();
                        break;
                    case 1:
                        Toast.makeText(MainActivity.this, "登入成功", Toast.LENGTH_SHORT).show();
                        Intent intent=new Intent();
                        intent.setClass(MainActivity.this,LobbyActivity.class);
                        intent.putExtra("user",user);
                        intent.putExtra("pass",pass);
                        intent.putExtra("PHPid",PHPid);
                        intent.putExtra("userID",ans.id);
                        startActivity(intent);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            // 中斷連線
            if( connection != null ) {
                connection.disconnect();
            }
        }
    }

    public class login{
        @SerializedName("status")
        public int check;
        @SerializedName("id")
        public String id;
    }

    //MD5 加密
    public static String MD5(String str) {
        String md5=null;
        try {
            MessageDigest md=MessageDigest.getInstance("MD5");
            byte[] barr=md.digest(str.getBytes());  //將 byte 陣列加密
            StringBuffer sb=new StringBuffer();  //將 byte 陣列轉成 16 進制
            for (int i=0; i < barr.length; i++) {sb.append(byte2Hex(barr[i]));}
            String hex=sb.toString();
            md5=hex.toUpperCase(); //一律轉成大寫
        }
        catch(Exception e) {e.printStackTrace();}
        return md5;
    }
    public static String byte2Hex(byte b) {
        String[] h={"0","1","2","3","4","5","6","7","8","9","a","b","c","d","e","f"};
        int i=b;
        if (i < 0) {i += 256;}
        return h[i/16] + h[i%16];
    }
}
