package com.example.user.game;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by user on 2016/8/2.
 */
public class NewGame extends AppCompatActivity{
    String[] flower={"clubs","diamonds","hearts","spades"};
    String[] num_e={"","ace","two","three","four","five","six","seven","eight","nine","ten","jack","queen","king"};
    String[] num={"","ace","2","3","4","5","6","7","8","9","10","jack","queen","king"};
    private TableLayout endLayout;
    private TableLayout gameLayout;
    TextView textView;
    OkHttpClient client = new OkHttpClient();
    FormBody formBody;
    Request request;
    GetMCard getMCard;
    GetPCard getPCard;
    SetMaker setMaker;
    GameResult gameResult;
    Get_Result get_Result;
    GameStart gameStart;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);
        Intent intent=getIntent();
        final String id=intent.getExtras().getString("user_id");
        final String PHPid=intent.getExtras().getString("PHPid");

        textView= (TextView) findViewById(R.id.text_response);
        gameLayout = (TableLayout) findViewById(R.id.game_btnLayout);
        endLayout= (TableLayout) findViewById(R.id.gameEnd_btnLayout);
        formBody = new FormBody.Builder()
                .add("id", id)
                .build();

        setTitle("Game Start!");

        try {
            GameStart(PHPid);
        } catch (IOException e) {
            e.printStackTrace();
        }
        ViewHolder holder=new ViewHolder();
        holder.btn_end.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    Button_End(PHPid);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        holder.btn_exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    if(ResetData(PHPid)==1){
                        finish();
                    }else{
                        Toast.makeText(NewGame.this, "無法登出，請稍後在試", Toast.LENGTH_SHORT).show();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        holder.btn_get.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    Button_Get(id,PHPid);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        holder.btn_golobby.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    if(ResetData(PHPid)==1){
                        finish();
                    }else{
                        Toast.makeText(NewGame.this, "無法回到大廳", Toast.LENGTH_SHORT).show();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        holder.btn_newgame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    if(ResetData(PHPid)==1){
                        GameStart(PHPid);
                        gameLayout.setVisibility(View.VISIBLE);
                        endLayout.setVisibility(View.GONE);
                    }else{
                        Toast.makeText(NewGame.this, "無法開新遊戲，請稍後在試", Toast.LENGTH_SHORT).show();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        holder.btn_change.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    if(ResetData(PHPid)==1){
                        GameStart(PHPid);
                    }else{
                        Toast.makeText(NewGame.this, "無法開新遊戲，請稍後在試", Toast.LENGTH_SHORT).show();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        //Thread錯誤糾正方式
        StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
                .detectDiskReads()
                .detectDiskWrites()
                .detectNetwork()   // or .detectAll() for all detectable problems
                .penaltyLog()
                .build());
        StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder()
                .detectLeakedSqlLiteObjects()
                .detectLeakedClosableObjects()
                .penaltyLog()
                .penaltyDeath()
                .build());
    }

    private void GameStart(final String PHPid) throws IOException {
        String url = "http://kevin84322.raguhn.tw/webgame/api/gamestart.php";

        final Request request = new Request.Builder()
                .url(url)
                .addHeader("COOKIE","PHPSESSID="+PHPid)
                .post(formBody)
                .build();
        Response response = client.newCall(request).execute();
        gameStart = new Gson().fromJson(response.body().charStream(), GameStart.class);

        if (gameStart.check == 1) {
            getMakerCard(PHPid);
            getPlayerCard(PHPid);
            textView.setText("遊戲開始，是否抽牌？");
        } else {
            Toast.makeText(NewGame.this, "發牌失敗", Toast.LENGTH_SHORT).show();
        }
        response.body().close();
    }

    public void getMakerCard(String PHPid) throws IOException {
        String url = "http://kevin84322.raguhn.tw/webgame/api/get_makercard.php";

        Request request = new Request.Builder()
                .url(url)
                .post(formBody)
                .addHeader("COOKIE","PHPSESSID="+PHPid)
                .build();
        Response mCard = client.newCall(request).execute();

        getMCard = new Gson().fromJson(mCard.body().charStream(), GetMCard.class);

        if (getMCard.check == 1) {

            String[] sp=(getMCard.gm_card.toString()).split("\\[|, |\\]");

            Adapter_Card adapter_card=new Adapter_Card(NewGame.this,Card(sp));
            RecyclerView recyclerView=(RecyclerView) findViewById(R.id.recycle_maker);

            MyLayoutManager layoutManager = new MyLayoutManager(NewGame.this);
            recyclerView.setLayoutManager(layoutManager);
            recyclerView.setAdapter(adapter_card);
        } else {
            Toast.makeText(NewGame.this, "莊家取牌失敗", Toast.LENGTH_SHORT).show();
        }
        mCard.body().close();
    }

    public void getPlayerCard(String PHPid) {
        String url = "http://kevin84322.raguhn.tw/webgame/api/get_playercard.php";

        final Request request = new Request.Builder()
                .url(url)
                .post(formBody)
                .addHeader("Cookie","PHPSESSID="+PHPid)
                .build();

        Response response = null;
        try {
            response = client.newCall(request).execute();
        } catch (IOException e) {
            e.printStackTrace();
        }
        getPCard = new Gson().fromJson(response.body().charStream(), GetPCard.class);
        if (getPCard.check == 1) {
            String[] sp=(getPCard.gp_card.toString()).split("\\[|, |\\]");

            Adapter_Card adapter_card=new Adapter_Card(NewGame.this,Card(sp));
            RecyclerView recyclerView=(RecyclerView)findViewById(R.id.recycle_player);
            MyLayoutManager layoutManager = new MyLayoutManager(NewGame.this);
            recyclerView.setLayoutManager(layoutManager);
            recyclerView.setAdapter(adapter_card);

//            Adapter_Card adapter_card=new Adapter_Card(NewGame.this,Card(sp));
//            RecyclerView recyclerView=(RecyclerView) findViewById(R.id.recycle_player);
//            LinearLayoutManager layoutManager = new LinearLayoutManager(NewGame.this);
//            layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
//            recyclerView.setLayoutManager(layoutManager);
//            recyclerView.setAdapter(adapter_card);

        } else {
            Toast.makeText(NewGame.this, "玩家取牌失敗", Toast.LENGTH_SHORT).show();
        }
        response.body().close();
    }

    public List Card(String[] sp) {
        //首先new一個items ArrayList容器物件，等一下拿來裝map用的
        ArrayList<HashMap<String, String>> items= new ArrayList<>();

        for (int i=0;i<sp.length;i++){
            if(!sp[i].equals("")){
                String[] token = sp[i].split("-");
                HashMap map = new HashMap<>();
                map.put("flower", token[0]);
                map.put("num", token[1]);
                items.add(map);
            }
        }
        String name;
        ArrayList img = new ArrayList();
        for (int i=0;i<items.size();i++){
            if(items.get(i).get("num").equals("99")){
                name="back_of_cards";
                img.add(name);
            }else{
                name=num[Integer.parseInt(items.get(i).get("num"))]+"_of_"+
                        flower[Integer.parseInt(items.get(i).get("flower"))];
                img.add(name);
            }
        }
        return img;
    }

    private void Login(String PHPid) throws IOException {
        String url = "http://kevin84322.raguhn.tw/webgame/api/check_login.php";

        final Request request = new Request.Builder()
                .url(url)
                .post(formBody)
                .addHeader("COOKIE","PHPSESSID="+PHPid)
                .build();

        Response response = client.newCall(request).execute();
        TextView text = (TextView) findViewById(R.id.text_response);
        String s=response.body().string();
        text.setText(s);
    }

    private void Button_Get(String id,String PHPid) throws IOException {
        String url = "http://kevin84322.raguhn.tw/webgame/api/get_card.php";

        FormBody formBody = new FormBody.Builder()
                .add("id", id)
                .add("takecard","1")
                .build();

        final Request request = new Request.Builder()
                .url(url)
                .post(formBody)
                .addHeader("Cookie","PHPSESSID="+PHPid)
                .build();

        Response response = client.newCall(request).execute();

        getPCard = new Gson().fromJson(response.body().charStream(), GetPCard.class);
        String[] sp;
        Adapter_Card adapter_card;
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recycle_player);
        MyLayoutManager layoutManager = new MyLayoutManager(NewGame.this);

        recyclerView.setLayoutManager(layoutManager);

        switch (getPCard.check){
            case 1:
                sp=(getPCard.gp_card.toString()).split("\\[|, |\\]");
                adapter_card=new Adapter_Card(NewGame.this,Card(sp));
                recyclerView.setAdapter(adapter_card);
                Toast.makeText(NewGame.this, "補牌", Toast.LENGTH_SHORT).show();
                textView.setText("抽牌成功，是否繼續？");
                break;
            case 21:
                gameLayout.setVisibility(View.GONE);

                sp=(getPCard.gp_card.toString()).split("\\[|, |\\]");
                adapter_card=new Adapter_Card(NewGame.this,Card(sp));
                recyclerView.setAdapter(adapter_card);

                if(GameResult(PHPid)==1){
                    Get_GameResult(PHPid);
                }
                break;
            case 5:
                gameLayout.setVisibility(View.GONE);

                sp=(getPCard.gp_card.toString()).split("\\[|, |\\]");
                adapter_card=new Adapter_Card(NewGame.this,Card(sp));

                recyclerView.setAdapter(adapter_card);
                if(GameResult(PHPid)==1){
                    Get_GameResult(PHPid);
                }
                break;
            case -87:
                Toast.makeText(NewGame.this, "玩家取牌失敗", Toast.LENGTH_SHORT).show();
                break;
        }
        response.body().close();
    }

    private void openDialog(String p_result, String game_result) {
        new AlertDialog.Builder(this)
                .setTitle(p_result)
                .setMessage(game_result)
                .setCancelable(false)   //點擊對話框以外的地方不會關閉
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                //按下按鈕後執行的動作，沒寫則退出Dialog
                                endLayout.setVisibility(View.VISIBLE);
                            }
                        }
                )
                .show();
    }

    private Integer ResetData(String PHPid) throws IOException {
        String url = "http://kevin84322.raguhn.tw/webgame/api/reset_gamedata.php";

        final Request request = new Request.Builder()
                .url(url)
                .post(formBody)
                .addHeader("Cookie","PHPSESSID="+PHPid)
                .build();
        Response response = client.newCall(request).execute();
        gameStart = new Gson().fromJson(response.body().charStream(), GameStart.class);
        int i=gameStart.check;
        response.body().close();
        return i;

    }

    private void Button_End(String PHPid) throws IOException {
        gameLayout.setVisibility(View.GONE);

        SetMaker(PHPid);
        if(GameResult(PHPid)==1){
            Get_GameResult(PHPid);
        }
    }

    private void Get_GameResult(String PHPid) throws IOException {
        String url = "http://kevin84322.raguhn.tw/webgame/api/get_gameresult.php";

        request = new Request.Builder()
                .url(url)
                .post(formBody)
                .addHeader("Cookie","PHPSESSID="+PHPid)
                .build();
        Response response = client.newCall(request).execute();

        get_Result =new Gson().fromJson(response.body().charStream(),Get_Result.class);

        if(get_Result.check==1){
            String[] sp=(get_Result.m_card.toString()).split("\\[|, |\\]");

            Adapter_Card adapter_card=new Adapter_Card(NewGame.this,Card(sp));
            RecyclerView recyclerView=(RecyclerView) findViewById(R.id.recycle_maker);
            MyLayoutManager layoutManager = new MyLayoutManager(NewGame.this);

            recyclerView.setLayoutManager(layoutManager);
            recyclerView.setAdapter(adapter_card);

            openDialog(get_Result.p_result,get_Result.game_result);

            textView.setText("莊家點數"+get_Result.m_point+"\n玩家點數"+get_Result.p_point);
        }else{
            Toast.makeText(NewGame.this, "計算錯誤", Toast.LENGTH_SHORT).show();
        }
        response.body().close();
    }

    private Integer GameResult(String PHPid) throws IOException {
        String url = "http://kevin84322.raguhn.tw/webgame/api/gameresult.php";

        Request request = new Request.Builder()
                .url(url)
                .post(formBody)
                .addHeader("Cookie","PHPSESSID="+PHPid)
                .build();
        Response response = client.newCall(request).execute();

        gameResult=new Gson().fromJson(response.body().charStream(),GameResult.class);
        int i=gameResult.check;
        response.body().close();
        return i;
    }

    private void SetMaker(String PHPid) throws IOException {
        String url = "http://kevin84322.raguhn.tw/webgame/api/set_makeraction.php";

        final Request request = new Request.Builder()
                .url(url)
                .post(formBody)
                .addHeader("Cookie","PHPSESSID="+PHPid)
                .build();
        Response response = client.newCall(request).execute();

        setMaker = new Gson().fromJson(response.body().charStream(),SetMaker.class);
        if (setMaker.check == 1) {
            String[] sp=(setMaker.e_card.toString()).split("\\[|, |\\]");

            Adapter_Card adapter_card=new Adapter_Card(NewGame.this,Card(sp));
            RecyclerView recyclerView=(RecyclerView) findViewById(R.id.recycle_maker);
            MyLayoutManager layoutManager = new MyLayoutManager(NewGame.this);

            recyclerView.setLayoutManager(layoutManager);
            recyclerView.setAdapter(adapter_card);
        } else {
            Toast.makeText(NewGame.this, "莊家取牌失敗", Toast.LENGTH_SHORT).show();
        }
        response.body().close();
    }


    public class ViewHolder {
        Button btn_get =(Button)findViewById(R.id.get);
        Button btn_exit =(Button)findViewById(R.id.exit);
        Button btn_end =(Button)findViewById(R.id.end);
        Button btn_golobby=(Button)findViewById(R.id.go_lobby);
        Button btn_newgame=(Button)findViewById(R.id.get_newGame);
        Button btn_change=(Button)findViewById(R.id.new_game);
    }
    private class GameStart {
        @SerializedName("status")
        public int check;
    }
    private class SetMaker {
        @SerializedName("status")
        public int check;
        @SerializedName("m_card")
        public List<String> e_card;
    }
    private class GetMCard {
        @SerializedName("status")
        public int check;
        @SerializedName("m_card")
        public List<List<String>> gm_card;
    }
    private class GetPCard {
        @SerializedName("status")
        public int check;
        @SerializedName("p_card")
        public List<List<String>> gp_card;
    }
    private class GameResult {
        @SerializedName("status")
        public int check;
    }
    private class Get_Result {
        @SerializedName("status")
        public int check;
        @SerializedName("p_point")
        public int p_point;
        @SerializedName("m_point")
        public int m_point;
        @SerializedName("m_card")
        public List<String> m_card;
        @SerializedName("p_result")
        public String p_result;
        @SerializedName("gameresult")
        public String game_result;
    }
}

