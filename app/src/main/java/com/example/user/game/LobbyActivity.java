package com.example.user.game;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.StrictMode;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;
import com.squareup.picasso.Picasso;

import java.io.IOException;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class LobbyActivity extends AppCompatActivity {
    OkHttpClient client = new OkHttpClient();
    Record record;
    User userName;
    Logout logout;
    private String id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lobby);

        //Thread錯誤糾正方式
        StrictMode
                .setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
                        .detectDiskReads()
                        .detectDiskWrites()
                        .detectNetwork()   // or .detectAll() for all detectable problems
                        .penaltyLog()
                        .build());
        StrictMode
                .setVmPolicy(new StrictMode.VmPolicy.Builder()
                        .detectLeakedSqlLiteObjects()
                        .detectLeakedClosableObjects()
                        .penaltyLog()
                        .penaltyDeath()
                        .build());
    }

    @Override
    protected void onResume() {
        super.onResume();
        Intent intent=getIntent();
        id=intent.getExtras().getString("userID");
        String PHPid=intent.getExtras().getString("PHPid");
        try {
            getRecord(id,PHPid);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void getRecord(String id, String PHPid) throws IOException {
        String url = "http://kevin84322.raguhn.tw/webgame/api/get_playerrecord.php";

        FormBody formBody = new FormBody.Builder()
                .add("id", id)
                .build();

        final Request request = new Request.Builder()
                .url(url)
                .post(formBody)
                .build();
        Response response=client.newCall(request).execute();
        record = new Gson().fromJson(response.body().charStream(), Record.class);
        if (record.check == 1) {
            try {
                getView(id,PHPid);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            Toast.makeText(LobbyActivity.this, "其它錯誤", Toast.LENGTH_SHORT).show();
        }
        response.body().close();
    }

    private String getUser(final String id) throws IOException {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        String s = null;
        String url = "http://kevin84322.raguhn.tw/webgame/api/get_playername.php";
        FormBody formBody = new FormBody.Builder()
                .add("id", id)
                .build();
        Request request = new Request.Builder()
                .url(url)
                .post(formBody)
                .build();

        Response response = client.newCall(request).execute();
        userName = new Gson().fromJson(response.body().charStream(), User.class);
        if (userName.check == 1) {
            s = userName.user_name;
        } else {
            Toast.makeText(LobbyActivity.this, "找不到使用者名稱", Toast.LENGTH_SHORT).show();
        }
        return s;
    }

    private class ViewHolder {
        TextView textView_name = (TextView) findViewById(R.id.text_name);
        TextView textView_win = (TextView) findViewById(R.id.text_win);
        TextView textView_lose=(TextView)findViewById(R.id.text_lose);
        TextView textView_tie=(TextView)findViewById(R.id.text_tie);
        TextView tv_winning=(TextView)findViewById(R.id.text_winning);
        Button button_exit=(Button)findViewById(R.id.exit);
        Button button_start=(Button)findViewById(R.id.start);
        ImageView imageView=(ImageView)findViewById(R.id.imageView);
    }
    private void getView(final String id, final String PHPid) throws IOException {
        ViewHolder holder = new ViewHolder();
        double win=record.d_win/((double)record.d_win+record.d_lose+record.d_tie);
        setTitle("Hi! "+getUser(id)+"恭喜你終於進來了");
        holder.textView_name.setText("玩家："+getUser(id));
        holder.textView_win.setText("勝場："+record.d_win);
        holder.textView_lose.setText("敗場："+record.d_lose);
        holder.textView_tie.setText("平手："+record.d_tie);
        holder.tv_winning.setText(getString(R.string.win_text, win*100));

        Picasso.with(LobbyActivity.this)
                .load(R.drawable.e70)
                .resize(150, 150)
                .into(holder.imageView);
        holder.button_start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setClass(LobbyActivity.this, NewGame.class);
                intent.putExtra("user_id",id);
                intent.putExtra("PHPid",PHPid);
                startActivity(intent);
            }
        });
        holder.button_exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    Logout(id);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void Logout(String id) throws IOException {
        String url="http://kevin84322.raguhn.tw/webgame/api/set_logout.php";
        FormBody formBody = new FormBody.Builder()
                .add("id", id)
                .build();
        Request request = new Request.Builder()
                .url(url)
                .post(formBody)
                .build();

        Response response = client.newCall(request).execute();
        logout= new Gson().fromJson(response.body().charStream(), Logout.class);
        if(logout.check==1){
            finish();
        }else{
            Toast.makeText(LobbyActivity.this, "登出失敗", Toast.LENGTH_SHORT).show();
        }
        response.body().close();
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if ((keyCode == KeyEvent.KEYCODE_BACK)) {   //確定按下退出鍵

            ConfirmExit(id); //呼叫ConfirmExit()函數
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    public void ConfirmExit(final String id){

        AlertDialog.Builder ad=new AlertDialog.Builder(LobbyActivity.this); //創建訊息方塊
        ad.setTitle("離開");
        ad.setMessage("確定要登出?");
        ad.setCancelable(false);
        ad.setPositiveButton("是", new DialogInterface.OnClickListener() { //按"是",則退出應用程式
            public void onClick(DialogInterface dialog, int i) {
                try {
                    Logout(id);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        ad.setNegativeButton("否",new DialogInterface.OnClickListener() { //按"否",則不執行任何操作
            public void onClick(DialogInterface dialog, int i) {
            }
        });
        ad.show();//顯示訊息視窗
    }

    private class Record {
        @SerializedName("status")
        public int check;
        @SerializedName("win_frequency")
        public int d_win;
        @SerializedName("lose_frequency")
        public int d_lose;
        @SerializedName("tie_frequency")
        public int d_tie;

    }
    private class User {
        @SerializedName("status")
        public int check;
        @SerializedName("user_name")
        public String user_name;
    }
    private class Logout {
        @SerializedName("status")
        public int check;
    }
}
